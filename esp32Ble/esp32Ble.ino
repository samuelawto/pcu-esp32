//Test1
#include "src/TOTP/sha1.h"
#include "src/TOTP/TOTP.h"

#include "src/Time/TimeLib.h"
#include "src/Time/Time.h"

#include "src/ESP32BLE/BLEDevice.h"
#include "src/ESP32BLE/BLEServer.h"
#include "src/ESP32BLE/BLEUtils.h"
#include "src/ESP32BLE/BLE2902.h"

#include "src/RTC/ThreeWire.h"
#include "src/RTC/RtcDS1302.h"

#include <require_cpp11.h>
#include <MFRC522.h>
#include <deprecated.h>
#include <MFRC522Extended.h>
#include <SPI.h>

//////////////////RFID PIN //////////////////////

#define SS_PIN    21
#define RST_PIN   22

///////////////////////RFID///////////////////////////

MFRC522 mfrc522(SS_PIN, RST_PIN); //Creamos el objeto para el RC522
bool card_find = false;
String incomingString = " ";
byte CardNumber[4];
byte CarKey[4] = {0xF6,0xAD,0x5D,0xF7};

//////////// RTC Configurations ///////////////
ThreeWire myWire(12,13,14); // IO, SCLK, CE
RtcDS1302<ThreeWire> rtc(myWire);

/////////// OTP CONFIGURATION //////////////////
uint8_t hmacKey[] = {0x41, 0x57, 0x54, 0x4f, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36}; //HEX KEY
TOTP totp = TOTP(hmacKey, 10,100);
char code[7];

//////////// BLE Configurations ///////////////
BLEServer *pServer = NULL;
BLECharacteristic *pCharacteristic;
bool deviceConnected = false;
bool oldDeviceConnected = false;
float txValue = 0;
String string_read ="";
String string_from_ble = "";
int wrong_code_count = 0;

///// Breath LED Configuration ////////
int freq = 5000;
int ledChannel = 0;
int resolution = 8;
int fade = 0;

//////////// PINS SETUP ///////////
int immo = 5;
int open_door = 27;
int close_door = 26;
const int inosatBoxImmo = 15;
portMUX_TYPE mux = portMUX_INITIALIZER_UNLOCKED;

/////////////////////////////7
 std::string co;

// See the following for generating UUIDs:
// https://www.uuidgenerator.net/

#define SERVICE_UUID "4C088507-7D53-4C1A-A21F-73085F340FB1" // UART service UUID; Este seria el serivicio que va a buscar la APP  
#define CHARACTERISTIC_UUID_RX "6E400002-B5A3-F393-E0A9-E50E24DCCA9E" // UUID Caracteristica RX
#define CHARACTERISTIC_UUID_TX "6E400003-B5A3-F393-E0A9-E50E24DCCA9E" // UUID Caracteristica TX 
#define LED_BUILTIN 17

//Call-Backs
// Connections call backs
class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      deviceConnected = true;
      Serial.println("Device connected");
      fade ++;
    };

    void onDisconnect(BLEServer* pServer) {
      deviceConnected = false;
      Serial.println("Device disconnected");
      fade = 0;
    };
};

// Reading Callbacks
class MyCallbacks: public BLECharacteristicCallbacks {
  
    void onWrite(BLECharacteristic *pCharacteristic) {
      
      std::string rxValue = pCharacteristic->getValue();
      if (rxValue.length() > 0) {
        Serial.println("Received Value: ");
        string_from_ble = rxValue.c_str();
        Serial.println(string_from_ble);
      }
    };
};

void IRAM_ATTR immoDetected() {
  int val = 0;
  portENTER_CRITICAL_ISR(&mux);
  Serial.print("se ha detectado un cambio de estado:");
  delay(1000);
  val = digitalRead(inosatBoxImmo);
  if (val == 0){
    executeAction(3);
  }else if (val == 1){
    executeAction(4);
  }
  Serial.println(val);
  portEXIT_CRITICAL_ISR(&mux);
}
//SETUP
void setup() {
  // declaracion de variables 
  pinMode(immo, OUTPUT);  // Pin para bloqueo de motor
  pinMode(open_door, OUTPUT); // pin para apertura de puertas
  pinMode(close_door, OUTPUT); // pin para cierre de puetas

//Edicion de valores inicales de HIGH a LOW

  digitalWrite(open_door, LOW); 
  digitalWrite(close_door, LOW); 
  digitalWrite(immo, LOW);

  pinMode(LED_BUILTIN, OUTPUT);
  ledcSetup(ledChannel, freq, resolution);
  ledcAttachPin(LED_BUILTIN, ledChannel); //LED_BUILTIN

  //////////////////////rfid/////////////
  SPI.begin();        //Iniciamos el Bus SPI
  mfrc522.PCD_Init();
  //////////////////////////////////////

  pinMode(inosatBoxImmo, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(inosatBoxImmo), immoDetected, CHANGE);
  
  Serial.begin(115200);

  // Create the BLE Device
  BLEDevice::init("AWTO"); // Give it a name

  // Create the BLE Server
  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  // Create the BLE Service
  BLEService *pService = pServer->createService(SERVICE_UUID);

  // Create a BLE Characteristic
  pCharacteristic = pService->createCharacteristic(
                      CHARACTERISTIC_UUID_TX,
                      BLECharacteristic::PROPERTY_NOTIFY
                    );
                      
  pCharacteristic->addDescriptor(new BLE2902());

  BLECharacteristic *pCharacteristic = pService->createCharacteristic(
                                         CHARACTERISTIC_UUID_RX,
                                         BLECharacteristic::PROPERTY_WRITE |
                                         BLECharacteristic::PROPERTY_READ
                                       );

  pCharacteristic->setCallbacks(new MyCallbacks());

  // Start the service
  pService->start();

  // Start advertising
  BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
  pAdvertising->addServiceUUID(SERVICE_UUID);
  pAdvertising->setScanResponse(true);
  pAdvertising->setMinPreferred(0x06);  // functions that help with iPhone connections issue
  pAdvertising->setMinPreferred(0x12);
  BLEDevice::startAdvertising();
  //pServer->getAdvertising()->start();
  Serial.println("BLE OK Waiting for a client...");
  
  // RTC initiation 
  rtc.Begin();
  RtcDateTime compiled_time = RtcDateTime(__DATE__, __TIME__); // Obtiene la hora y fecha en que se compila el dispositivo.
  
  if (rtc.GetIsWriteProtected()){
    Serial.println("RTC was write protected, enabling writing now");
    rtc.SetIsWriteProtected(false);
  }
  if (!rtc.GetIsRunning()){
    Serial.println("RTC was not actively running, starting now");
    rtc.SetIsRunning(true);
  }

  //Rutina para reloj 
  RtcDateTime rtc_time_now = rtc.GetDateTime();
  if (rtc_time_now < compiled_time) 
    {
        Serial.println("RTC is older than compilation time!  (Updating DateTime)");
        rtc.SetDateTime(compiled_time);
        rtc_time_now = rtc.GetDateTime();
    }
  setTime(rtc_time_now.Epoch32Time()); //
  Serial.print("Tiempo del reloj en timestamp: "); // 
  Serial.println(rtc_time_now.Epoch32Time());
}

void loop() {
  /////////////////////////////
  fadeFunc(); // Funcion para apagado y encendido de led de STATUS de conexion 
  ////////////////////////////

  ////////SERIAL PORT READ//////////////
   if (Serial.available() > 0) {
    string_read = Serial.readString(); // read the incoming byte:
    readingPort(string_read);
   }

   if (string_from_ble.length() > 0){
    readingPort(string_from_ble);
   }
  if (deviceConnected) {
    //Here you can put any code needed to check while connection is made.
  }

  // disconnecting
  if (!deviceConnected && oldDeviceConnected) {
    delay(500); // give the bluetooth stack the chance to get things ready
    pServer->startAdvertising(); // restart advertising
    Serial.println("start advertising");    
    oldDeviceConnected = deviceConnected;
  }
  // connecting
  if (deviceConnected && !oldDeviceConnected) {
    // do stuff here on connecting
    oldDeviceConnected = deviceConnected;
  }

  char* newCode = totp.getCode(now());
  if(strcmp(code, newCode) != 0) {
    strcpy(code, newCode);
    Serial.println(code);
  }
}

void readingPort(String s){ 
  if (s.startsWith("getTime")){  //Cuando el modulo lee la frase GETTIME este envia al dispositivo el Timestap que tiene
      getTime();      
    }else {
      String received_code = s.substring(0,6); // se recibe por puerto serial el codigo OTP enviado por el dispositivo
      if (checkOTP(received_code)){  // Se verifica si el codigo OTP conincide con el condogo interno.
        wrong_code_count = 0;
        Serial.println("¡CODE OK!, AUTH COMMAND");
        String action_received = s.substring(7); // verifcamos el septimo caracter
        executeAction(action_received.toInt()); // Nos vamos a la rutina de ejecutar comandos
      }else {  // Si el OTP es incorrecto se envia codigo de ERROR
        wrong_code_count++;
        std::string co;
        if (wrong_code_count == 1){  //Al primer error enviamos 205 (OTP equivocado)
          printBoth(co = "205"); 
        }else if (wrong_code_count == 2){ // AL segundo error enviamos 205,18F (OTP equivocado, desconectar dispositivo)
          printBoth(co = "205,18F");
          wrong_code_count = 0;
          disconnectDevice(); // se desconecta dispositivo
        }
      }
    }
    string_from_ble = "";
}
// Rutina para validad OTP
bool checkOTP(String otp_r){
  char* newCode = totp.getCode(now());
  String mystring(newCode);
  if (otp_r != mystring){
    return false;
  }else{
    return true;
  }  
}

//Rutina de ejecutar comandos 
void executeAction(int command){
 
  switch (command){                              
    case 1:  // si el comando es 1 se abren las puertas
      digitalWrite(open_door, HIGH);
      delay(1000);
      digitalWrite(open_door, LOW);
      printBoth(co = "200,DO" );
      getTime();
      break;
    case 2: // si el comando es 2 se cierran las puertas
      digitalWrite(close_door, HIGH);
      delay(1000);
      digitalWrite(close_door, LOW);
      printBoth(co = "200,DC");
      getTime();
      break;
    case 3:  // si el comando es 3 se bloquea el motor 
      digitalWrite(immo, HIGH);
      printBoth(co = "200,ION");
      getTime();
      break;
    case 4:  // si el comando es 4 se desbloquea el motor 
      digitalWrite(immo,LOW );
      printBoth(co = "200,IOFF");
      getTime();
      break;
    case 5:   // si el comando es 5 se lee el RFID
      readCard();
      Serial.println("Leer key detector");
      break;
    default:
      printBoth(co = "180");
  }
  
}

// Rutina para obtener el TimeStampd
void getTime(){
  std::string time_to_send;
  time_to_send = "ts:";
  time_to_send += String(now()).c_str();
  printBoth(time_to_send);
}
// Desconectar dispositivo 
void disconnectDevice(){
  pServer->disconnectClient();
}

// Rutina imprime las respuestas y las envia al dispositivo
void printBoth(std::string text_to_print){
  Serial.println(text_to_print.c_str());
  char txString[8]; // make sure this is big enuffz
  dtostrf(txValue, 1, 2, txString); // float_val, min_width, digits_after_decimal, char_buffer
  pCharacteristic->setValue(text_to_print); // Sending a test message
  pCharacteristic->notify(); // Send the value to the app!
}

//Funcion de estatus de conecxion de BLE 
void fadeFunc(){
  if (fade>0){
    int dutyCycle = 255;
    ledcWrite(ledChannel, dutyCycle);
    delay(10);
    }else{
      for (int dutyCycle = 0; dutyCycle <= 255; dutyCycle++) {
        ledcWrite(ledChannel, dutyCycle);
        delay(10);
      }
      for (int dutyCycle = 255; dutyCycle >= 0; dutyCycle--) {
        ledcWrite(ledChannel, dutyCycle);
        delay(10);
      }
      delay(30);
    }
}

///////////////////////////////////////////////
/////////////////////RFID READ ////////////////
///////////////////////////////////////////////

void readCard(){
  int cont = 0;
  while (cont < 10){
    Serial.println("leyendo...");
    if (mfrc522.PICC_IsNewCardPresent()){
      if (mfrc522.PICC_ReadCardSerial()){
        // Enviamos serialemente su UID
        Serial.print("Leemos Card UID:");
        for (byte i = 0; i < mfrc522.uid.size; i++) {
          Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
          Serial.print(mfrc522.uid.uidByte[i], HEX);
          CardNumber[i] = mfrc522.uid.uidByte[i];
          card_find = true;
        }
        Serial.println();
        // Terminamos la lectura de la tarjeta  actual
        mfrc522.PICC_HaltA();
     }
    }
    cont++;
      delay(10);
  }
//// RFID PARA SUSTITUIR KEY DETECTOR POR TAG RFID
  if (compareCards() && card_find){
    Serial.println("llave encontrada");
    printBoth(co = "200,llave encontrada");
    mfrc522.PCD_SoftPowerDown();
  }else{
    Serial.println("llave no encontrada");
    printBoth(co = "200,llave no encontrada");
  }
  card_find=false;
}

bool compareCards(){
  bool same_card = true;
  for (byte i = 0; i<4;i++){
    if (CardNumber[i] != CarKey[i]){
      same_card = false;
    }
  }
  return same_card;
}
